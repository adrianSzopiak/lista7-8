import com.basho.riak.client.api.RiakClient;
import com.basho.riak.client.api.commands.kv.DeleteValue;
import com.basho.riak.client.api.commands.kv.FetchValue;
import com.basho.riak.client.api.commands.kv.StoreValue;
import com.basho.riak.client.core.RiakCluster;
import com.basho.riak.client.core.RiakNode;
import com.basho.riak.client.core.query.Location;
import com.basho.riak.client.core.query.Namespace;
import com.basho.riak.client.core.query.RiakObject;
import com.basho.riak.client.core.util.BinaryValue;

import java.util.concurrent.ExecutionException;

public class app {

    private static RiakCluster riakCluster;
    private static RiakClient riakClient;
    private static final String BUCKET = "objects";

    public static void main(String[] args) {
        initRiak();
        try {
            addDoc();
            printDoc();
            updateDoc();
            printDoc();
            deleteDoc();
            printDoc();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void initRiak() {
        RiakNode node = new RiakNode.Builder()
                .withRemoteAddress("127.0.0.1")
                .withRemotePort(8087)
                .build();
        riakCluster = new RiakCluster.Builder(node).build();
        riakCluster.start();
        riakClient = new RiakClient(riakCluster);
    }

    private static void addDoc() throws ExecutionException, InterruptedException {
        RiakObject object = new RiakObject()
                .setContentType("text/plain")
                .setValue(BinaryValue.create("My new Document with value: 123"));
        Location location = new Location(new Namespace(BUCKET), "first");
        StoreValue storeOp = new StoreValue.Builder(object).withLocation(location).build();
        StoreValue.Response response = riakClient.execute(storeOp);
        System.out.println("Add response: " + response.toString());
    }

    private static RiakObject getDoc() throws ExecutionException, InterruptedException {
        FetchValue fetchOp = new FetchValue.Builder(new Location(new Namespace(BUCKET), "first")).build();
        return riakClient.execute(fetchOp).getValue(RiakObject.class);
    }

    private static void printDoc() throws ExecutionException, InterruptedException {
        RiakObject riakObject = getDoc();
        System.out.println("Get response: " + (riakObject == null ? "null" : riakObject.getValue().toString()));
    }

    private static void updateDoc() throws ExecutionException, InterruptedException {
        RiakObject object = getDoc();
        object.setValue(BinaryValue.create("This is modified value !!!"));
        StoreValue updateValue = new StoreValue.Builder(object)
                .withLocation(new Location(new Namespace(BUCKET), "first"))
                .build();
        System.out.println("Update response: " + riakClient.execute(updateValue).toString());
    }

    private static void deleteDoc() throws ExecutionException, InterruptedException {
        DeleteValue deleteOp = new DeleteValue.Builder(new Location(new Namespace(BUCKET), "first")).build();
        System.out.println("Delete Response: " + riakClient.execute(deleteOp));
    }
}
